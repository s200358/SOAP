package helloservice;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.acme.Person;

import java.util.List;

public class SimpleDTUPay {

	WebTarget baseUrl;

	public SimpleDTUPay() {
		Client client = ClientBuilder.newClient();
		baseUrl = client.target("http://localhost:8081/");
	}
	
	public String hello() {
		return baseUrl.path("hello").request().get(String.class);
	}


	public String pay(String cid, String mid, int amount) {
		return baseUrl.path("dtupay")
				.path("initiatePayment")
				.queryParam("cid", cid)
				.queryParam("mid", mid)
				.queryParam("amount", amount)
				.request(MediaType.APPLICATION_JSON_TYPE)
				.post(Entity.json(null), String.class);
	}
	public void getPayments() {
	}
}
