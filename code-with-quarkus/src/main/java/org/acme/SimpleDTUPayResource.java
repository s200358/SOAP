package org.acme;

import org.jboss.resteasy.annotations.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/dtupay")
public class SimpleDTUPayResource {
    List<Payment> payments = new ArrayList<>();
    String customerId = "cid1";
    String merchantId = "mid1";
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello DTUPay";
    }
    
    @POST
    @Path("initiatePayment")
    @Produces(MediaType.APPLICATION_JSON)
    public Response initiatePayment(@QueryParam("cid") String cid, @QueryParam("mid") String mid, @QueryParam("amount") double amount) {
        System.out.println("initate");
        if (!merchantId.equals(mid)) {
            return Response.status(Response.Status.NOT_FOUND).entity(PaymentStatus.UNKNOWN_MERCHANT).build();
        } else if (!customerId.equals(cid)) {
            return Response.status(Response.Status.NOT_FOUND).entity(PaymentStatus.UNKNOWN_CUSTOMER).build();
        }
    	payments.add(new Payment(cid, mid, amount));
        return Response.ok(PaymentStatus.SUCCESSFUL).build();
    }
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Payment> getPaymentList() {
    	return payments;
    }
}