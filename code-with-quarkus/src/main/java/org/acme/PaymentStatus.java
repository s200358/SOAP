package org.acme;

public enum PaymentStatus {
    SUCCESSFUL,
    FAILED,
    UNKNOWN_CUSTOMER,
    UNKNOWN_MERCHANT
}
